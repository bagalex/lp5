<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(PostsTableSeeder::class);
        $this->call(SubscribersTableSeeder::class);
    }
}

class PostsTableSeeder extends Seeder
{

    public function run()
    {
        factory(App\Post::class,30)->create();
    }
}

class SubscribersTableSeeder extends Seeder
{

    public function run()
    {
        factory(App\Subscriber::class,50)->create();
    }
}