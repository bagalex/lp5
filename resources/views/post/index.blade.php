@extends('layouts.app')

@section('content')
    <ul>
        @foreach($posts as $post)
            <li><a href="{{ url('/post/' . $post->id) }}">{{ $post->title }}</a></li>
        @endforeach
    </ul>
    {{ $posts->links() }}
@endsection