@extends('layouts.app')

@section('content')
    <div class="input-group input-group-lg">
        <form action="{{ route('subscriber.store') }}" method="POST">
            <input type="hidden" method="PATCH">
            {{ csrf_field() }}

            <div class="input-group">
                <input name="email" type="text" class='form-control' placeholder="Your email">
                <span class="input-group-btn"><button class="btn btn-default" type="submit">Subscribe!</button></span>
            </div>
        </form>
    </div>
@endsection