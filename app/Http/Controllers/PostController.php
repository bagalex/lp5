<?php

namespace App\Http\Controllers;

use App\Repositories\PostRepository;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * The PostRepository instance.
     *
     * @var PostRepository
     */
    protected $post_repo;

    /**
     * Create a new ContactController instance.
     *
     * @param PostRepository $post_repo
     */
    public function __construct(PostRepository $post_repo)
    {
        $this->post_repo = $post_repo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts=  $this->post_repo->paginate();

        return view('post.index', compact('posts'));
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function show(\App\Post $post)
    {
        return view('post.show', compact('post'));
    }
}
