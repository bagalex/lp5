<?php

namespace App\Http\Controllers;

use App\Http\Requests\SubscriberRequest;

use App\Repositories\SubscriberRepository;
use Illuminate\Http\Request;

class SubscriberController extends Controller
{
    /**
     * The SubscriberRepository instance.
     *
     * @var SubscriberRepository
     */
    protected $subscriber_repo;

    /**
     * Create a new ContactController instance.
     *
     * @param SubscriberRepository $subscriber_repo
     */
    public function __construct(SubscriberRepository $subscriber_repo)
    {
        $this->subscriber_repo = $subscriber_repo;
    }

    /**
     * Display a listing of the resource.
     *
     * @param SubscriberRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubscriberRequest $request)
    {
        $this->subscriber_repo->create($request->all());

        return redirect()->back()->withMessage('Success');
    }
}